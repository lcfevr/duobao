 <?php
     // NGINX环境变量TP_ENV , 定义APP_STATUS应用状态
     if (isset($_SERVER['TP_ENV'])) {
         $tp_env = strtolower(trim($_SERVER['TP_ENV']));
         define('APP_STATUS', $tp_env);
     }

     include_once "./".APP_STATUS.".php";

     $appname = APP_NAME;
     $apiurl = Api_Url;
?>

var _appname=<?php echo "'".$appname."'" ?>;
var _apiurl=<?php echo "'".$apiurl."'" ?>;
