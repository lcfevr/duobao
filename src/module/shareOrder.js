/**
 * Created by Sherry on 2017/1/22.
 */
// import Config from '../config/config'
import Request from '../config/request'
class ShareOrder{
    constructor(allData={},details={}){
        this.allData=allData;
        this.details=details;
    }

    async getList(id = 0,page = 0,limit = 10){

        let key = 'id' + id + 'page' + page + 'limit' + limit;

        if (Config.isCache && !!this.allData[key]) {
            return this.allData[key];
        }

        let res = await Request.post(Config.apiDomain + '/Shareorder/all',{data: {
            id: id,
            offset: limit * page,
            limit: limit
        }})

        if (res.status ==200) {
            this.allData[key] = res.data;
            return res.data;
        }

    }

    async getListByUID(id=0, page=0, limit=10){

        let key =  'id' + id + 'page' + page + 'limit' + limit;

        if (Config.isCache && !!this.allData[key]) {

            return (this.allData[key]);
        }

        let res = await Request.post(Config.apiDomain + '/Shareorder/getUserShareByUID',{data: {
            id: id,
            offset: limit * page,
            limit: limit
        }});


        if (res.status == 200){

            this.allData[key] = res.data;

            return res.data;

        }

    }

    async getShareDetail(id){

        if (!!this.details[id]) {

            return this.details[id]

        }

        let res = await Request.post(Config.apiDomain + '/Shareorder/getShareOrderById',{data:{id:id}})

        if (res.status == 200) {

            this.details[id] = !!res.data.item ? res.data.item[0] : null;

            return this.details[id];

        } else {

            return false;

        }

    }



}


let shareOrder =new ShareOrder();
export default shareOrder;