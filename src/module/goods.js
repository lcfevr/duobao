/**
 * Created by Sherry on 2016/12/30.
 */
// import Config from '../config/config'
import Request from '../config/request'
import md5Hex from 'md5-hex'
import Util from '../libs/util'
class Goods {
    constructor( allData={},detail={},intro={},code={},record={},previousRecord={},catgory=null,freeGoods=null,calculate={}){
        this.allData=allData;
        this.detail=detail;
        this.intro=intro;
        this.code=code;
        this.record=record;
        this.previousRecord=previousRecord;
        this.catgory=catgory;
        this.freeGoods=freeGoods;
        this.calculate=calculate;
    }

   async getList(formData,query) {

        let key =query;


        // categoryid:商品类别id,传0选择所有类别的商品，默认为0
        // offset: 偏移量，默认为0
        // limit: 数据长度，默认为10
        // ordername:排序规则，可选值[1,2,3] 1:人气 2：上架时间 3：剩余人次
        // order:总需人数的排序,可选值[desc,asc];

        let res=await Request.post(Config.apiDomain + '/Goods/allGoods',{data:formData});

        if(res.status==200){
            // this.allData[key]=res.data;
        }

       return res.data.item;


    }
   async getCatgory(){
    if(!!this.catgory){

        return this.catgory;
    }

        let res=await Request.post(Config.apiDomain + '/Goods/getCategory');
        if(res.status==200){
            this.catgory=res.data;
            console.log(res.data);
            return res.data;
        }


    }
   async getFreeGoods(){
    if(!!this.freeGoods){

        return  this.freeGoods;
    }

       let res=await Request.post(Config.apiDomain + '/Goods/getFreeGoods');
       if (res.status == 200) {
           this.freeGoods = res.data;
           return res.data;
       }

    }
   async getDetail(id,token){

       let res=await Request.post(Config.apiDomain + '/Goods/getGoodsById',{data:{id:id,token:token}});
       if (res.status == 200) {
           console.log(res)
           if(res.before){
               this.detail[id] = res.data.message;
               return {data:res.data.message,before:res.before.message}
           }
           else {
               this.detail[id] = res.data.message;
               return {data:res.data.message};
           }

       }else{
           return false;
       }

    }
    async getInfo(goodid,token){
        let res=await Request.post(Config.apiDomain + '/goods/getBuyCode',{data:{id:goodid,token:token}});
        console.log(goodid);
        console.log(token);
        if (res.status == 200) {
            self.code[goodid]=res.data.winBuyCode;
          return res.data.winBuyCode;
        }else{
            return res;
        }

    }
   async getRecommond(){

       let res=await Request.post(Config.apiDomain + '/goods/getSuggest');
       if (res.status == 200) return res.data;

    }
   async addShareRecod(uid,id,fun_name){
        let code = md5Hex(uid+id+fun_name);
       let res=await Request.post(Config.apiDomain + '/Goods/addShareRecode',{data:{
           uid:uid,
           id:id,
           fun_name:fun_name,
           code:code
       }});

       return res;

    }
    async getIntro(id){

        if(!!this.intro[id]){
            return this.intro[id];

        }

        let res=await Request.post( Config.apiDomain + '/Goods/getGoodContentById',{data:{id:id}});
        if (res.status == 200) {
            this.intro[id] = res.data;
            return res.data;
        }


    }
    async getPreviousRecord(param){
        let id = param.offset;
        if(!!this.previousRecord[id]){
           return this.previousRecord[id];
            return;
        }

        let res=await Request.post( Config.apiDomain + '/Goods/previousRecord',{data:param});
        if (res.status == 200) {
            this.previousRecord[id] = res.data;
           return res.data;
        }
    }
    async getRecord(formData){

       let key = Util.objToQueryString(formData);

        if(!!this.record[key]){
            return this.record[key];
        }

        let res=await Request.post( Config.apiDomain + '/Goods/inRecordOfGoods',{data:formData});
        if (res.status == 200) {
            this.record[key] = res.data;
            return res.data;
        }
    }

    async getCheckCalculateDetail(gid){

        if(!!this.calculate[gid]){

            return  this.calculate[gid];
        }
        let res=await Request.get(Config.apiDomain + '/Goods/checkCalculateDetail?gid='+gid);
        if (res.status == 200) {
            this.calculate[gid] = res.data;
           return res.data;
        }
    }
    async getLatest(formData){
        let res = await Request.get(Config.apiDomain + '/Goods/get_latest?offset='+formData.offset+'&limit='+formData.limit);
        if (res.status == 200) return res.data;

    }
    async getFreeTime(){
        let res=await Request.get(Config.apiDomain + '/index/getFreeTime');
        if (res.status == 200) {
           return res.data;
        }
    }
    async getRedpack(type){

        let res=await Request.post(Config.apiDomain + '/Goods/checkCalculateDetail',{data:{s_type:type}});
        if (res.status == 200) {
         return res;
        }

    }
    async getGroupProduct(goodid){
        let res=await Request.post(Config.apiDomain + '/pingtuan/product',{data:{productid : Number.parseInt(goodid)}});
        return res;
    }

    async getGroupLists(page,pageSize){
        let res = await Request.post(Config.apiDomain + '/pingtuan/lists',{data: {
            page:page,
            pagesize:pageSize
        }});
        
        return res;
    }

   

}

let goods=new Goods();
export default goods;