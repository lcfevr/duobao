/**
 * Created by lcfevr on 16/7/4.
 */
/**
* 千位分隔符
* */
function numfilter (value) {
    return value.toString().replace(/\B(?=(\d{3})+$)/g,',');
}

function toInt(value){
    return ~~value;
}


export {
    toInt,
    numfilter
}