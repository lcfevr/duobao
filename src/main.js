/**
 * Created by lcfevr on 16/6/20.
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './components/app.vue';
import Routers from './router';
import Env from './config/env';
import VueResource from 'vue-resource';
import User from './module/user';
import Weixin from './libs/wxShare';

Vue.use(VueRouter);
Vue.use(VueResource);


Vue.http.options.xhr = {withCredentials: true}

// post的时候会把JSON对象转成formdata
Vue.http.options.emulateJSON=true;
Vue.http.options.emulateHTTP = true;
// 开启debug模式
Vue.config.debug = true;

Vue.prototype.cnzz = function(a){
    _czc.push(['_trackEvent',a , '点击','', '','']);
};

// 路由配置
let router = new VueRouter({
    // 是否开启History模式的路由,默认开发环境开启,生产环境不开启。如果生产环境的服务端没有进行相关配置,请慎用
    // history: Env != 'production'
    history:true
});

router.map(Routers);


//路由切换前做登录状态判定，非登录状态跳转到登录页面
router.beforeEach(({to,next}) => {
    window.scrollTo(0, 0);
    switch (to.path){
        // case '/login':
        case '/pay':
        case '/payback':
            if(!!User.isLogin)
                next();
             else
                router.go({ path: '/home' });

            return;
            break;
    }
    next()
});

router.afterEach(({to,from}) => {
    let pathname = to.matched[0].handler.path;
    // if(!!from.matched){
        Weixin.init();
    // }
    // _czc.push(﻿["_trackPageview","/download/thunder5.0.exe","http://www.mysite.com/list/"])
    if(!!from.matched) {
        _czc.push(﻿["_trackPageview", pathname, (location.origin + pathname)])
    }
});

router.redirect({
    '*': "/home"
});
router.start(App, '#app');