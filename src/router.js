/**
 * Created by lcfevr on 16/8/22.
 */
const routers = {
    '/index': {
        component (resolve) {
            require(['./views/index'], resolve);
        }
    },
    '/home': {
        component (resolve) {
            require(['./views/home'], resolve);
        }
    },
    '/list': {
        component (resolve) {
            require(['./views/list'], resolve);
        }
    },

    '/detail': {
        component (resolve) {
            require(['./views/detail'], resolve);
        }
    },
    '/cart': {
        component (resolve) {
            require(['./views/cart'], resolve);
        }
    },
    '/pay': {
        component (resolve) {
            require(['./views/pay'], resolve);
        }
    },
    '/payback': {
        component (resolve) {
            require(['./views/payback'], resolve);
        }
    },
    '/previousRecord': {
        component (resolve) {
            require(['./views/previousRecord'], resolve);
        }
    },
    '/intro': {
        component (resolve) {
            require(['./views/intro'], resolve);
        }
    },
    '/serveRule': {
            component (resolve) {
                require(['./views/serveRule'], resolve);
            }
    },
    '/calculation': {
            component (resolve) {
                require(['./views/calculation'], resolve);
            }
    },
    '/latest': {
            component (resolve) {
                require(['./views/latest'], resolve);
            }
    },
    '/winDetail': {
            name:'winDetail',
            component (resolve) {
                require(['./views/winDetail'], resolve);
            }
    },
    '/mall': {
        component (resolve) {
            require(['./views/mall'], resolve);
        }
    },
    '/scoreItem': {
        component (resolve) {
            require(['./views/scoreItem'], resolve);
        }
    },
    '/score': {
        component (resolve) {
            require(['./views/score'], resolve);
        }
    },

    '/serviceCenter': {
        component (resolve) {
            require(['./views/serviceCenter'], resolve);
        }
    },
    '/serviceDetail': {
        component (resolve) {
            require(['./views/serviceCenterItem'], resolve);
        }
    },


    /*
       Share
     */

    '/shareRecord': {
        component (resolve) {
            require(['./views/shareRecord'], resolve);
        }
    },

    '/shareOrder': {
        component (resolve) {
            require(['./views/shareOrder'], resolve);
        }
    },

    '/shareDetail': {
        component (resolve) {
            require(['./views/shareDetail'], resolve);
        }
    },

    '/shareEdit': {
        component (resolve) {
            require(['./views/shareEdit'], resolve);
        }
    },

    /*

    User

     */
    '/user': {
        component (resolve) {
            require(['./views/user'], resolve);
        }
    },

    '/coupon': {
        component (resolve) {
            require(['./views/coupon'], resolve);
        }
    },
     '/myShareOrder': {
            component (resolve) {
                require(['./views/myShareOrder'], resolve);
            }
        },


    '/login': {
        component (resolve) {
            require(['./views/login'], resolve);
        }
    },

    '/address': {
        component (resolve) {
            require(['./views/address'], resolve);
        }

    },
    '/userDetail': {
        component (resolve) {
            require(['./views/userDetail'], resolve);
        }
    },
    '/buyRecord': {
        component (resolve) {
            require(['./views/buyRecord'], resolve);
        }
    },
    '/winRecord': {
        component (resolve) {
            require(['./views/winRecord'], resolve);
        }
    },
    '/groupRecord': {
        component (resolve) {
            require(['./views/groupRecord'], resolve);
        }
    },
    '/chargeRecord': {
        component (resolve) {
            require(['./views/chargeRecord'], resolve);
        }
    },
    '/charge': {
        component (resolve) {
            require(['./views/charge'], resolve);
        }
        
    },
    '/profile':{
        name:'profile',
        component (resolve) {
            require(['./views/profile'], resolve);
        }
    },
    '/groupLists': {
        component (resolve) {
            require(['./views/groupLists'], resolve);
        }
    },
    '/groupHome': {
        component (resolve) {
            require(['./views/groupHome'], resolve);
        }
    },
    '/groupPayback': {
        component (resolve) {
            require(['./views/groupPayback'], resolve);
        }
    },
    '/groupPay': {
        component (resolve) {
            require(['./views/groupPay'], resolve);
        }
    },
    
    // 活动

    '/act1': {
        component (resolve) {
            require(['./views/act1'], resolve);
        }
    },

    '/act2': {
        component (resolve) {
            require(['./views/act2'], resolve);
        }
    },

    '/act3': {
        component (resolve) {
            require(['./views/act3'], resolve);
        }
    },


    //免费红包
    '/act4': {
        component (resolve) {
            require(['./views/act4'], resolve);
        }
    },
};
export default routers;