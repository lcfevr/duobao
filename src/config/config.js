/**
 * Created by lcfevr on 16/7/18.
 */
import Env from './env';

let config = {
    env: Env,
    appName: '邵阳夺宝',
    apiDomain: 'http://api2.shaoyangduobao.weizhuanqiandao.com',
    imgDomain: '',
    isCache: true,
    serverCode: {
        // 请求成功
        '200': '成功'
    },
    serverUrl:'https://xiaoyuzhonglian.qiyukf.com/client?k=aa47dc1efad0c73daaf95b57224201e5&wp=1'
};
export default config;