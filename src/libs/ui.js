/**
 * Created by Sherry on 2016/12/22.
 */

export default{
    loadingNumber:0,
    loading(vm,n){
        if (!isNaN(n) && n < 0 && --this.loadingNumber <= 0) {
            this.loadingNumber = 0;
            vm.isLoading=false;
        } else {
            if (isNaN(n) || n > 0) {
                this.loadingNumber++;
            }
            vm.isLoading=true;
        }
    },


};