/**
 * Created by Sherry on 2016/9/27.
 */
// import config from '../config/config';
import Request from '../config/request';
  var shareWx={
        isWeixin: "micromessenger" == window.navigator.userAgent.toLowerCase().match(/MicroMessenger/i),
        pcWeixin: "windowswechat" == window.navigator.userAgent.toLowerCase().match(/WindowsWechat/i),
        signatureInterface:'/Weixin/getSignPackage',
        confInterface:'/Common/shareInterface',
        shareConf: {
          title: "众筹梦想！惊喜无限！",
          desc: "我已参与夺宝，点击马上有惊喜!!!",
          link: (function(){
              return window.location.origin +'/index';
          })(),
          imgUrl: "https://img.alicdn.com/imgextra/i2/1724390189/TB2_qQvaSOI.eBjSspmXXatOVXa_!!1724390189.jpg",
          success: function() {
              
          },
          cancel: function() {}
        },
        async init(callback){
            var _this=this,
                setings = "onMenuShareTimeline onMenuShareAppMessage onMenuShareQQ onMenuShareWeibo onMenuShareQZone startRecord stopRecord onVoiceRecordEnd playVoice pauseVoice stopVoice onVoicePlayEnd uploadVoice downloadVoice chooseImage previewImage uploadImage downloadImage translateVoice getNetworkType openLocation getLocation hideOptionMenu showOptionMenu hideMenuItems showMenuItems hideAllNonBaseMenuItem showAllNonBaseMenuItem closeWindow scanQRCode chooseWXPay openProductSpecificView addCard chooseCard openCard";
            let res=await Request.post(Config.apiDomain+this.signatureInterface,{data:{'url':encodeURIComponent(location.href.replace(/#.*$/, ""))}});
            if(res.status!=200){
                return false;
            }
            wx.config({
                debug: 0,
                appId: res.data.signature.appId,
                timestamp: res.data.signature.timestamp,
                nonceStr: res.data.signature.nonceStr,
                signature: res.data.signature.signature,
                jsApiList: setings.split(" ")
            });
            wx.ready(function() {
                if(callback){
                    callback();
                }
                else{
                    _this.updateShare()
                }
                // wx.hideOptionMenu();
            });
            wx.error(function(res) {
                console.log('微信验证失败');
            });
          
        },
        async getConf(){
            this.updateShare();
            //
            // var resovle=await Request.post(config.apiDomain+this.confInterface,{data:{'url':encodeURIComponent(location.href.replace(/#.*$/, ""))}});
            // if(resovle.status==200){
            //     console.log(this.shareConf.title)
            //     this.shareConf.title=resovle.data.title;
            //     console.log(resovle.data)
            //
            //     this.shareConf.desc=resovle.data.desc;
            //     this.shareConf.imgUrl=resovle.data.imageurl;
            //     this.updateShare();
            //     return true;
            // }
            //
            // return false;

        },
        updateShare:function(conf){
            var setings = "onMenuShareTimeline onMenuShareAppMessage onMenuShareQQ onMenuShareWeibo onMenuShareQZone",
                thisConf = this.shareConf,
                obj = {},
                Conf = conf || {};
            for (var k in thisConf) {
                if (Conf.hasOwnProperty(k)) {
                    obj[k] = Conf[k];
                } else {
                    obj[k] = thisConf[k];
                }
            }
            console.log(obj)

            setings.split(" ").forEach(function(e) {
                if(e == 'onMenuShareTimeline'){
                    wx[e]({
                        title: obj['desc'],
                        link: obj['link'],
                        imgUrl: obj['imgUrl'],
                        fun_name: 'onMenuShareTimeline',
                        success: obj['success'],
                        cancel: obj['cancel']
                    });
                }else if(e=='onMenuShareAppMessage'){
                    wx[e]({
                        title: obj['title'],
                        link: obj['link'],
                        desc:obj['desc'],
                        imgUrl: obj['imgUrl'],
                        fun_name: 'onMenuShareAppMessage',
                        success: obj['success'],
                        cancel: obj['cancel']
                    });
                } else {
                    obj['fun_name'] = e;
                    wx[e](obj);
                }
            });
        },
        staticstical:function(alias_event, action) {  //cnzz方法
        var Action = action || 'onMenuShareTimeline';
        _czc.push(["_trackEvent",alias_event,Action,"daka","sign",config.link]);

        }
};

export default shareWx;